import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DescriptionComponent } from './components/description/description.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'description', component: DescriptionComponent },
  { path: 'dishes', loadChildren: './dish/dish.module#DishModule' },
  { path: 'regions', loadChildren: './region/region.module#RegionModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
