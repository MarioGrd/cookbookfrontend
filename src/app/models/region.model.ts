export interface IRegion {
    id: string;
    originalName: string;
    englishName: string;
    shortName: string;
    otherNames: string;
    regionLevelId: number;
    popularity: 0;
    dishCount: 0;
    ingredientCount: 0;
}