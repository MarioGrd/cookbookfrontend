export interface IDishQuery {
    query: string;
    pageNumber: number;
    pageSize: number;
}