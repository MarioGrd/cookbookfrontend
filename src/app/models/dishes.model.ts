import { IDish } from "./dish.model";

export interface IDishes {
    data: IDish[];
    pageNumber: number;
    pageSize: number;
    count: number;
}