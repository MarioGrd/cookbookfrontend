export interface IAutocompleteResult {
    id: string;
    name: string;
}