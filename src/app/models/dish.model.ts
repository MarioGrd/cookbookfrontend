export interface IDish {
    id: string;
    originalName: string;
    englishName: string;
    shortName: string;
    otherNames: string;
    description: string;
    popularity: number;
    commentCount: number;
    userRating: number;
}