export interface IRegionQuery {
    query: string;
    pageNumber: number;
    pageSize: number;
}