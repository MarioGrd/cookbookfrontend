export interface IAutocomplete {
    query: string;
    take: number;
}