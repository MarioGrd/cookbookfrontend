export interface IAddDishCommand {
    id: string;
    originalName: string;
    englishName?: string;
    otherNames?: string;
    shortName?: string;
    popularity: number;
    description: string;
}