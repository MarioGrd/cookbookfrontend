export class AddDishCommand {
    originalName: string;
    popularity: number;
    description: string;
    englishName?: string;
    otherNames?: string;
    shortName?: string;
}