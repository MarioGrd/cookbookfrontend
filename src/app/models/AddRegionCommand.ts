export interface IAddRegionCommand {
    originalName: string;
    englishName?: string;
    otherNames?: string;
    shortName?: string;
    popularity: number;
    regionLevelId: number;
}