import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { DishService } from "src/app/services/dish.service";

@Injectable()
export class DishUpdateResolver implements Resolve<any> {

    constructor(private dishService: DishService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.dishService.getDish(route.params['id']);
    }
}