import { NgModule } from '@angular/core';

import { DishRoutingModule } from './dish-routing.module';
import { DishesComponent } from './views/dishes/dishes.component';
import { DishAddComponent } from './components/dish-add/dish-add.component';
import { DishListComponent } from './components/dish-list/dish-list.component';
import { DishSearchComponent } from './components/dish-search/dish-search.component';
import { DishUpdateComponent } from './components/dish-update/dish-update.component';
import { DishHomeComponent } from './components/dish-home/dish-home.component';
import { SharedModule } from '../shared/shared.module';
import { DishUpdateResolver } from './resolvers/DishUpdateResolver';

@NgModule({
  declarations: [DishesComponent, DishAddComponent, DishListComponent, DishSearchComponent, DishUpdateComponent, DishHomeComponent],
  providers: [DishUpdateResolver],
  imports: [
    SharedModule,
    DishRoutingModule,
  ]
})
export class DishModule { }
