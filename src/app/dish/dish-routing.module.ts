import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DishesComponent } from './views/dishes/dishes.component';
import { DishUpdateComponent } from './components/dish-update/dish-update.component';
import { DishSearchComponent } from './components/dish-search/dish-search.component';
import { DishAddComponent } from './components/dish-add/dish-add.component';
import { DishHomeComponent } from './components/dish-home/dish-home.component';
import { DishUpdateResolver } from './resolvers/DishUpdateResolver';

const routes: Routes = [  
{ path: '', component: DishesComponent, children: [
  { path: 'home', component: DishHomeComponent },
  { path: 'search', component: DishSearchComponent },
  { path: ':id/update', component: DishUpdateComponent, resolve: {dish: DishUpdateResolver} },
  { path: 'add', component: DishAddComponent }
] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishRoutingModule { }
