import { Component } from '@angular/core';
import { IAddDishCommand } from 'src/app/models/IAddDishCommand';
import { NgForm } from '@angular/forms';
import { DishService } from 'src/app/services/dish.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dish-add',
  templateUrl: './dish-add.component.html',
  styleUrls: ['./dish-add.component.scss']
})
export class DishAddComponent {

  model: IAddDishCommand = { id: '', originalName: null, englishName: '', shortName: '', otherNames: '', popularity: 0, description: '' };

  constructor(private dishService: DishService, private router: Router) { }

  onSubmit(form: NgForm) {

    if (form.invalid) return;

    this.dishService.addDish(form.value).subscribe(() => this.router.navigate(['dishes', 'search']));
  }
}
