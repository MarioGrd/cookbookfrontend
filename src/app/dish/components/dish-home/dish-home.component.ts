import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DishService } from 'src/app/services/dish.service';
import { IDish } from 'src/app/models/dish.model';

@Component({
  selector: 'app-dish-home',
  templateUrl: './dish-home.component.html',
  styleUrls: ['./dish-home.component.scss']
})
export class DishHomeComponent implements OnInit {

  dishes$: Observable<IDish[]>;

  constructor(private dishService: DishService) { }

  ngOnInit() {
    this.onLoadMore(1);
    this.dishes$ = this.dishService.dishSubject;
  }

  onLoadMore(page: number) {
    this.dishService.getDishes({ query: '', pageNumber: page, pageSize: 10 });
  }

}
