import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishHomeComponent } from './dish-home.component';

describe('DishHomeComponent', () => {
  let component: DishHomeComponent;
  let fixture: ComponentFixture<DishHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
