import { Component } from '@angular/core';
import { IAddDishCommand } from 'src/app/models/IAddDishCommand';
import { DishService } from 'src/app/services/dish.service';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dish-update',
  templateUrl: './dish-update.component.html',
  styleUrls: ['./dish-update.component.scss']
})
export class DishUpdateComponent {

  model: IAddDishCommand = { id: '', originalName: null, englishName: '', shortName: '', otherNames: '', popularity: 0, description: ''};

  constructor(private dishService: DishService,  private router: Router, private activatedRoute: ActivatedRoute) { 
    this.activatedRoute.data.subscribe(s => this.model = s.dish);
  }

  onSubmit(form: NgForm) {

    if (form.invalid) return;

    this.dishService.updateDish(this.model).subscribe(() => this.router.navigate(['dishes', 'search']));
  }
}
