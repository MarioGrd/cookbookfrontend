import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IDish } from 'src/app/models/dish.model';
import { Observable } from 'rxjs';
import { DishService } from 'src/app/services/dish.service';

@Component({
  selector: 'app-dish-search',
  templateUrl: './dish-search.component.html',
  styleUrls: ['./dish-search.component.scss']
})
export class DishSearchComponent implements OnInit {
  
  @ViewChild('search') search: ElementRef;
  dishes$: Observable<IDish[]>;
  query: string = '';
  searchText$: Observable<any>;

  constructor(private dishService: DishService) { }

  ngOnInit() {
    this.onSearch();
    this.dishes$ = this.dishService.dishSubject;

/*     this.searchText$ =
      fromEvent<any>(this.search.nativeElement, 'keyup')
        .pipe(
          map(event => event.target.value),
          debounceTime(400),
          distinctUntilChanged()
        );
    this.dishes$ = this.searchText$.pipe(
      switchMap(search => this.dishService.getDishes({ query: search, pageNumber: 1, pageSize: 10 }))
    ); */
  }

  onSearch() {
    this.dishService.getDishes({ query: this.query, pageNumber: 1, pageSize: 10 });
  }

}
