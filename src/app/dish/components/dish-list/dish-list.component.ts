import { Component, OnInit, Input } from '@angular/core';
import { IDish } from 'src/app/models/dish.model';
import { DishService } from 'src/app/services/dish.service';

@Component({
  selector: 'app-dish-list',
  templateUrl: './dish-list.component.html',
  styleUrls: ['./dish-list.component.scss']
})
export class DishListComponent {

  @Input() dishes: IDish[];

  constructor(private dishService: DishService) { }

  onDishDelete(id: string){
    this.dishService.deleteDish(id);
  } 

}
