import { Component, OnInit } from '@angular/core';
import { DishService } from 'src/app/services/dish.service';
import { IDish } from 'src/app/models/dish.model';
import { map, catchError, filter } from 'rxjs/operators';
import { IDishes } from 'src/app/models/dishes.model';
import { throwError, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

}
