import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IDishes } from '../models/dishes.model';
import { IDishQuery } from '../models/dish-query.model';
import { Observable, throwError, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IDish } from '../models/dish.model';
import { IAddDishCommand } from '../models/IAddDishCommand';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  public dishSubject: Subject<IDish[]> = new Subject<IDish[]>();
  private dishes: IDish[] = [];

  constructor(private http: HttpClient) { }

  getDish(id: string): Observable<IDish> {

    return this.http.get<IDish>('https://localhost:44355/api/dish/' + id)
      .pipe(
        map((response: IDish) => response),
        catchError(error => throwError(error))
      )
  }

  getDishes(query:IDishQuery) {

    const dishParams = new HttpParams().set('query', query.query).set('pageNumber', query.pageNumber.toString()).set('pageSize', query.pageSize.toString());

    this.http.get<IDishes>('https://localhost:44355/api/dish/list', { params: dishParams })
      .pipe(
        map((response: IDishes) => response.data),
        catchError((error) => throwError(error))
      )
      .subscribe((data: IDish[]) => {
        this.dishes = data;
        this.dishSubject.next(this.dishes)
      })
  }

  addDish(command: IAddDishCommand) : Observable<boolean> {

    return this.http.post<IAddDishCommand>('https://localhost:44355/api/dish', command)
      .pipe(
        map(response => true),
        catchError(error => throwError(error))
      );
  }

  updateDish(command: IAddDishCommand) : Observable<boolean> {

    return this.http.put<IAddDishCommand>('https://localhost:44355/api/dish', command)
      .pipe(
        map(response => true),
        catchError(error => throwError(error))
      );
  }

  deleteDish(id: string) {
    const filtered = this.dishes.filter(dish => dish.id !== id);
    this.dishes = filtered;
    this.dishSubject.next(this.dishes);
  }
}
