import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { IAutocomplete } from '../models/IAutocomplete';
import { IAutocompleteResult } from '../models/IAutocompleteResult';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable, of } from 'rxjs';
import { IRegionQuery } from '../models/IRegionQuery';
import { Paged } from '../models/paged.model';
import { IRegion } from '../models/region.model';
import { IAddRegionCommand } from '../models/AddRegionCommand';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(private http: HttpClient) { }

  getRegion(id: string): Observable<IRegion> {
    return this.http.get<IRegion>('https://localhost:44355/api/region/' + id)
      .pipe(
        map((response: IRegion) => response),
        catchError(error => throwError(error))
      );
  }

  autocompleteRegion(autocomplete: IAutocomplete): Observable<IAutocompleteResult[]> {

    if (!autocomplete.query) return of([]);

    const params = new HttpParams().set('query', autocomplete.query).set('take', autocomplete.take.toString());

    return this.http.get<IAutocompleteResult[]>('https://localhost:44355/api/autocomplete/regions', { headers: null, params: params })
      .pipe(
        map((response: IAutocompleteResult[]) => {
          return response;
        }),
        catchError((error) => throwError(error))
      );
  }

  getRegions(query: IRegionQuery) : Observable<IRegion[]> {

    const regionParams = new HttpParams().set('query', query.query).set('pageNumber', query.pageNumber.toString()).set('pageSize', query.pageSize.toString());

    return this.http.get<Paged<IRegion>>('https://localhost:44355/api/region/list', { params: regionParams })
      .pipe(
        map((response: Paged<IRegion>) => response.data),
        catchError((error) => throwError(error))
      );
  }

  addRegion(command: IAddRegionCommand): Observable<boolean> {

    return this.http.post<IAddRegionCommand>('https://localhost:44355/api/region', command)
      .pipe(
        map(response => true),
        catchError(error => throwError(error))
      )
  }

  updateRegion(command: IAddRegionCommand): Observable<boolean> {

    return this.http.put<IAddRegionCommand>('https://localhost:44355/api/region', command)
      .pipe(
        map(response => true),
        catchError(error => throwError(error))
      )
  }
}
