import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { RegionService } from "src/app/services/region.service";

@Injectable()
export class RegionUpdateResolver implements Resolve<any> {

    constructor(private regionService: RegionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.regionService.getRegion(route.params['id']);
    }
}