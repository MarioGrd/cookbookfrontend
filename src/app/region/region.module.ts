import { NgModule } from '@angular/core';
import { RegionListComponent } from './components/region-list/region-list.component';
import { RegionSearchComponent } from './components/region-search/region-search.component';
import { RegionAddComponent } from './components/region-add/region-add.component';
import { RegionsComponent } from './views/regions/regions.component';
import { RegionRoutingModule } from './region-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RegionHomeComponent } from './components/region-home/region-home.component';
import { RegionUpdateComponent } from './components/region-update/region-update.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegionAddAltComponent } from './components/region-add-alt/region-add-alt.component';
import { RegionUpdateResolver } from './resolvers/RegionUpdateResolver';

@NgModule({
  declarations: [RegionListComponent, RegionSearchComponent, RegionAddComponent, RegionsComponent, RegionHomeComponent, RegionUpdateComponent, RegionAddAltComponent],
  providers: [RegionUpdateResolver],
  imports: [
    SharedModule,
    RegionRoutingModule,
    ReactiveFormsModule
  ]
})
export class RegionModule { }
