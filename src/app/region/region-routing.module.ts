import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegionsComponent } from './views/regions/regions.component';
import { RegionSearchComponent } from './components/region-search/region-search.component';
import { RegionAddComponent } from './components/region-add/region-add.component';
import { RegionUpdateComponent } from './components/region-update/region-update.component';
import { RegionHomeComponent } from './components/region-home/region-home.component';
import { RegionAddAltComponent } from './components/region-add-alt/region-add-alt.component';
import { RegionUpdateResolver } from './resolvers/RegionUpdateResolver';


const routes: Routes = [
    { path: '', component: RegionsComponent, children: [
        { path: 'home', component: RegionHomeComponent },
        { path: 'search', component: RegionSearchComponent },
        { path: 'add', component: RegionAddComponent },
        { path: 'addalt', component: RegionAddAltComponent},
        { path: ':id/update', component: RegionUpdateComponent, resolve: {region: RegionUpdateResolver} }
    ] },

  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class RegionRoutingModule { }