import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RegionService } from 'src/app/services/region.service';
import { IAddRegionCommand } from 'src/app/models/AddRegionCommand';
import { fromEvent } from 'rxjs';
import { concat, concatMap, exhaustMap } from 'rxjs/operators';

@Component({
  selector: 'app-region-add-alt',
  templateUrl: './region-add-alt.component.html',
  styleUrls: ['./region-add-alt.component.scss']
})
export class RegionAddAltComponent implements OnInit {

  @ViewChild('regionSaveBtn') regionSaveBtn: ElementRef;
  regionLevels = [
    {  id: 1, value: 'City' }, 
    {  id: 2, value: 'Region' }, 
    {  id: 3, value: 'Country' }, 
    {  id: 4, value: 'Continent' }];

  regionForm = new FormGroup({
    originalName: new FormControl('', [Validators.required]),
    englishName: new FormControl(''),
    otherNames: new FormControl(''),
    shortName: new FormControl(''),
    popularity: new FormControl('', [Validators.required]),
    regionLevelId: new FormControl('', [Validators.required])
  });

  constructor(private regionService: RegionService) { }

  ngOnInit() {
    fromEvent(this.regionSaveBtn.nativeElement, 'click')
      .pipe(
        exhaustMap(() => this.regionService.addRegion(this.regionForm.value))
      ).subscribe(s => console.log(s))
  }
}
