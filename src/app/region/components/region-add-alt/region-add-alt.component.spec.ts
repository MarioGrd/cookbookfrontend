import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionAddAltComponent } from './region-add-alt.component';

describe('RegionAddAltComponent', () => {
  let component: RegionAddAltComponent;
  let fixture: ComponentFixture<RegionAddAltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionAddAltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionAddAltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
