import { Component, OnInit } from '@angular/core';
import { RegionService } from 'src/app/services/region.service';
import { IRegion } from 'src/app/models/region.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-region-home',
  templateUrl: './region-home.component.html',
  styleUrls: ['./region-home.component.scss']
})
export class RegionHomeComponent implements OnInit {


  regions$: Observable<IRegion[]>;

  constructor(private regionService: RegionService) { }

  ngOnInit() {
    this.onLoadMore(1);
  }

  onLoadMore(page: number) {
    this.regions$ = this.regionService.getRegions({query: '', pageNumber: page, pageSize: 10});
  }

}
