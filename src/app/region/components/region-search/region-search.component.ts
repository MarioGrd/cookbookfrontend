import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IRegion } from 'src/app/models/region.model';
import { Observable, fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { RegionService } from 'src/app/services/region.service';

@Component({
  selector: 'app-region-search',
  templateUrl: './region-search.component.html',
  styleUrls: ['./region-search.component.scss']
})
export class RegionSearchComponent implements OnInit {

  @ViewChild('search') search: ElementRef;
  regions$: Observable<IRegion[]>;

  constructor(private regionService: RegionService) { }

  ngOnInit() {

    this.regions$ =
      fromEvent<any>(this.search.nativeElement, 'keyup')
        .pipe(
          map(event => event.target.value),
          debounceTime(400),
          distinctUntilChanged(),
          switchMap(search => this.regionService.getRegions({ query: search, pageNumber: 1, pageSize: 10 }))
        );
  }
}
