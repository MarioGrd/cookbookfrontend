import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm, FormBuilder } from '@angular/forms';
import { RegionService } from 'src/app/services/region.service';
import { IAddRegionCommand } from 'src/app/models/AddRegionCommand';
import { Router } from '@angular/router';

@Component({
  selector: 'app-region-add',
  templateUrl: './region-add.component.html',
  styleUrls: ['./region-add.component.scss']
})
export class RegionAddComponent {

  @ViewChild('regionSave') regionSaveBtn: ElementRef;
  regionLevels = [
    {  id: 1, value: 'City' }, 
    {  id: 2, value: 'Region' }, 
    {  id: 3, value: 'Country' }, 
    {  id: 4, value: 'Continent' }];

  regionForm = new FormGroup({
    originalName: new FormControl('', [Validators.required]),
    englishName: new FormControl(''),
    otherNames: new FormControl(''),
    shortName: new FormControl(''),
    popularity: new FormControl('', [Validators.required]),
    regionLevelId: new FormControl('', [Validators.required])
  });

  constructor(private regionService: RegionService, private router: Router) { }

  onSubmit() {

    if (this.regionForm.invalid) return;

    const regionCommand: IAddRegionCommand = this.regionForm.value;

    this.regionService.addRegion(regionCommand).subscribe(s => this.router.navigate(['regions', 'search']));
  }
}
