import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IAddRegionCommand } from 'src/app/models/AddRegionCommand';
import { RegionService } from 'src/app/services/region.service';

@Component({
  selector: 'app-region-update',
  templateUrl: './region-update.component.html',
  styleUrls: ['./region-update.component.scss']
})
export class RegionUpdateComponent implements OnInit {


  regionForm = new FormGroup({
    id: new FormControl('', [Validators.required]),
    originalName: new FormControl('', [Validators.required]),
    englishName: new FormControl(''),
    otherNames: new FormControl(''),
    shortName: new FormControl(''),
    popularity: new FormControl('', [Validators.required]),
    regionLevelId: new FormControl('', [Validators.required])
  });

  regionLevels = [
    {  id: 1, value: 'City' }, 
    {  id: 2, value: 'Region' }, 
    {  id: 3, value: 'Country' }, 
    {  id: 4, value: 'Continent' }];

  constructor(private activatedRoute: ActivatedRoute, private regionService: RegionService, private router: Router) {
    this.activatedRoute.data.subscribe(s => this.regionForm.patchValue(s.region));
   }

  ngOnInit() {
  }

  onSubmit() {

    if (this.regionForm.invalid) return;

    const regionCommand: IAddRegionCommand = this.regionForm.value;

    this.regionService.updateRegion(regionCommand).subscribe(s => this.router.navigate(['regions', 'search']));
  }

}
