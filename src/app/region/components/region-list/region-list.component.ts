import { Component, OnInit, Input } from '@angular/core';
import { IRegion } from 'src/app/models/region.model';

@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent  {

  @Input() regions: IRegion[];

  constructor() { }
}
