import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RegionLevelPipe } from './pipes/region-level.pipe';

@NgModule({
  declarations: [RegionLevelPipe],
  imports: [
  ],
  exports: [
    CommonModule,
    FormsModule,
    RegionLevelPipe
  ]
})
export class SharedModule { }
