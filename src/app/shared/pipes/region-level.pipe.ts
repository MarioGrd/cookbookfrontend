import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'regionLevel'
})
export class RegionLevelPipe implements PipeTransform {

  transform(value: number, args?: any): string {

    if (!value) return 'Nan'
    if (value === 1) {
      return 'City';
    } else if (value === 2) {
      return 'Region'
    }
    else if (value === 3) {
      return 'Country'
    } else {
      return 'Continent';
    }

  }

}
